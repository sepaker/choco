<div class="indicator-view">
    <h1>Акции: </h1>
    <table class="table table-bordered table-hover report">
        <tbody>
            <tr>
                <th>ID акции</th>
                <th>Название акции</th>
                <th>Дата начала акции</th>
                <th>Дата окончания</th>
                <th>Статус</th>
            </tr>
            <?php foreach($models as $item):?>
                <tr>
                    <td><?= $item->id ?></td>
                    <td><a href="/stock/show?slug=<?= $item->slug?>"><?= $item->title ?></a></td>
                    <td><?= $item->startDate ?></td>
                    <td><?= $item->endDate ?></td>
                    <td><?= $item->status ?></td>
                </tr>
            <?php endforeach;?> 
        </tbody>
    </table>
</div>