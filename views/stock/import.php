<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?= Yii::$app->session->getFlash('result'); ?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['enctype' => 'multipart/form-data']
]) ?>
 
    <div class="form-group">
        <input type="file" name="file">
    </div>
    <div class="form-group">
        <div>
            <?= Html::submitButton('Загрузить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>