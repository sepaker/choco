<?php

namespace app\models;

use Yii;
use app\components\Slug;
use app\components\Year;
use app\components\Status;
/**
 * This is the model class for table "stock".
 *
 * @property integer $id
 * @property string $title
 * @property string $start_date
 * @property string $end_date
 * @property integer $is_active
 */
class Stock extends \yii\db\ActiveRecord
{
    public $startDate;
    public $endDate;
    public $status;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock';
    }
    
    public function behaviors() 
    {
        return [
            'slug' => [
                'class' => Slug::className(),
                'name' => 'title',
                'slug' => 'slug'
            ],
            'start' => [
                'class' => Year::className(),
                'oldDate' => 'start_date',
                'newDate' => 'startDate'
            ],
            'end' => [
                'class' => Year::className(),
                'oldDate' => 'end_date',
                'newDate' => 'endDate'
            ],
            'status' => [
                'class' => Status::className(),
                'oldStatus' => 'is_active',
                'newStatus' => 'status'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'start_date', 'end_date', 'is_active'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['is_active'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название акции',
            'slug' => 'Ссылка',
            'start_date' => 'Дата начала акции',
            'end_date' => 'Дата окончания',
            'is_active' => 'Статус',
        ];
    }
}
