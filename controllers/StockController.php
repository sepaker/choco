<?php

namespace app\controllers;

use Yii;
use app\models\Stock;
use app\models\StockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StockController implements the CRUD actions for Stock model.
 */
class StockController extends Controller
{
    public function beforeAction($action) {
        parent::beforeAction($action);
        if(Yii::$app->db->schema->getTableSchema('stock'))
            return true;
        else
            return false;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stock models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stock();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Stock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Stock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionShow($slug)
    {
        return $this->render('view', [
            'model' => $this->findModelBySlug($slug),
        ]);
    }
    
    public function actionList()
    {
        return $this->render('list', [
           'models' => Stock::find()->all() 
        ]);
    }
    
    /**
     * CSV import 
     * По нормальному стоит использовать ext подключив его в composer.json 
     * "ruskid/yii2-csv-importer": "dev-master"
     */
    public function actionImport()
    {
        if(Yii::$app->request->isPost) {
            $f = fopen($_FILES['file']['tmp_name'], 'r');
            $data = fgetcsv($f, 1000, ';');
            while(!feof($f)) {
                $data = fgetcsv($f, 1000, ';');
                $data[0] = (int) $data[0];
                $data[0] = addslashes(trim($data[0]));
                if($this->findModel($data[0])){
                    Yii::$app->getSession()->setFlash('result', 'Данные успешно загружены');
                    return $this->render('import');
                }                    
                $data[1] = addslashes(trim($data[1]));
                $data[1] = iconv('CP1251', 'UTF-8', $data[1]);
                $data[2] = addslashes(trim($data[2]));
                $data[3] = addslashes(trim($data[3]));
                $data[4] = addslashes(trim($data[4]));
                $model = new Stock;
                $model->id = $data[0];
                $model->title = $data[1];
                $model->start_date = date_format(date_create($data[2]), 'Y-m-d');
                $model->end_date = date_format(date_create($data[3]), 'Y-m-d');
                $model->is_active = $data[4] == "On" ? 1 : 0;
                $model->save();
            }
            Yii::$app->getSession()->setFlash('result', 'Данные успешно загружены');
        }
        return $this->render('import');
    }
    
    public function actionRandom()
    {
        return $this->render('list', [
            'models' => Stock::find()->orderBy('RAND()')->limit(1)->all()
        ]);
    }

    /**
     * Finds the Stock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    function findModelBySlug($slug)
    {
        if (($model = Stock::find()->where("slug='$slug'")->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
