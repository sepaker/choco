<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;
use yii\db\ActiveRecord;
use yii\base\Behavior;

/**
 * Description of Years
 *
 * @author Каиржан
 */
class Year extends Behavior 
{
    public $oldDate;
    public $newDate;
    public $oldStatus;
    public $newStatus;
    
    public function events() 
    {
        return 
        [
            ActiveRecord::EVENT_AFTER_FIND => 'getDate',
        ];
    }
    
    public function getDate($event)
    {
        return $this->owner->{$this->newDate} = $this->generateDate($this->owner->{$this->oldDate});
    }
    
    public function generateDate($date)
    {
        return date('d.m.Y', strtotime($date));
    }
    
    
}