<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\components;
use yii\db\ActiveRecord;
use yii\base\Behavior;

/**
 * Description of Status
 *
 * @author Каиржан
 */
class Status extends Behavior 
{
    public $oldStatus;
    public $newStatus;
    
    public function events() 
    {
        return 
        [
            ActiveRecord::EVENT_AFTER_FIND => 'getStatus',
        ];
    }
    
    public function getStatus($event)
    {
        return $this->owner->{$this->newStatus} = $this->generateStatus($this->owner->{$this->oldStatus});
    }
    
    public function generateStatus($status)
    {
        return $status==1 ? 'On' : 'Off';
    }
}
