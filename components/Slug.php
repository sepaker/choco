<?php
/*
 * Лучше использовать ext подклюив его в composer.json
 * "2amigos/yii2-transliterator-helper": "*"
 */

namespace app\components;
use yii\db\ActiveRecord;
use yii\base\Behavior;
/**
 * Description of Slug
 * Translit helper
 * @author Каиржан
 */
class Slug extends Behavior 
{
    public $name;
    public $slug;
    public $traslit = [
        "а" => "a",
        "ый" => "iy",
        "ые" => "ie",
        "б" => "b",
        "в" => "v",
        "г" => "g",
        "д" => "d",
        "е" => "e",
        "ё" => "yo",
        "ж" => "zh",
        "з" => "z",
        "и" => "i",
        "й" => "y",
        "к" => "k",
        "л" => "l",
        "м" => "m",
        "н" => "n",
        "о" => "o",
        "п" => "p",
        "р" => "r",
        "с" => "s",
        "т" => "t",
        "у" => "u",
        "ф" => "f",
        "х" => "kh",
        "ц" => "ts",
        "ч" => "ch",
        "ш" => "sh",
        "щ" => "shch",
        "ь" => "",
        "ы" => "y",
        "ъ" => "",
        "э" => "e",
        "ю" => "yu",
        "я" => "ya",
        "йо" => "yo",
        "ї" => "yi",
        "і" => "i",
        "є" => "ye",
        "ґ" => "g"
    ];
    
    public function events()
    {
        return 
        [
            ActiveRecord::EVENT_BEFORE_INSERT => 'getSlug',
        ];
    }
    
    public function getSlug($event)
    {
        return $this->owner->{$this->slug} = $this->generateSlug($this->owner->{$this->name});
    }

    public function generateSlug($name)
    {
        $name = preg_replace('/-{2,}/', '-',
                    mb_strtolower(
                            trim(
                                preg_replace('/\s+/', '-', $name), '-')
                    )
                );
        return strtr($name,$this->traslit);
    }
}
